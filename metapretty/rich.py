class Text(object):
    """Rich text, that can be presented through the terminal or the GUI.
    TODO: actual richness lol"""
    def __init__(self, contents: str) -> None:
        self.contents = contents

    def __repr__(self):
        return self.contents

    def __str__(self):
        return self.contents