from typing import Tuple, Dict

class OS(object):
    """A known operating system."""

class OSSelector(object):
    """A selector for operating systems."""

class Installable(object):
    """A tool that can be installed."""

class PackageSpec(object):
    def __init__(self,
                 name: str,
                 version: str,
                 **overrides: Dict[OS, Tuple[str, str]]) -> None:
        self.name = name

class PackageManager(object):
    def __init__(self, name: str) -> None:
        self.name: str = name

    def short_info(self, spec: PackageSpec) -> str:
        raise NotImplementedError(f'short_info not implemented for package manager {self.name}')
    
    def is_installed(self, spec: PackageSpec) -> bool:
        raise NotImplementedError(f'is_installed not implemented for package manager {self.name}')

    def install(self, spec: PackageSpec):
        raise NotImplementedError(f'install not implemented for package manager {self.name}')
    
    def uninstall(self, spec: PackageSpec) -> str:
        raise NotImplementedError(f'uninstall not implemented for package manager {self.name}')


