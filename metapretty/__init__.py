# -*- coding: utf-8 -*-

"""Top-level package for metapretty."""

__author__ = """James Gilles"""
__email__ = 'jameshgilles@gmail.com'
__version__ = '0.1.0'
