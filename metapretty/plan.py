from typing import List
from metapretty import rich

class Action(object):
    def explain(self) -> rich.Text:
        raise NotImplementedError(f'short_explain not implemented for action {type(self).__name__}')

    def run(self):
        raise NotImplementedError(f'run not implemented for action {type(self).__name__}')

    @property
    def can_undo(self) -> bool:
        return False
    
    def undo(self):
        raise NotImplementedError(f'undo not implemented for action {type(self).__name__}')

class RunAction(Action):
    def __init__(self, command: str, args: List[str], desc: rich.Text, **popen_args) -> None:
        self.command = command
        self.args = args
        self.desc = desc
        self.popen_args = popen_args
    
    def explain(self) -> rich.Text:
        pass
        #return rich.Text(f"`{self.command} {' '.join("'" + a + "'" for a in self.args)} # {self.desc}`")


class CopyAction(Action):
    pass

class OverwriteAction(Action):
    pass

class GitCloneAction(Action):
    pass

class Plan(object):
    def __init__(self, actions: List[Action]) -> None:
        self.actions = actions



