==========
metapretty
==========


.. image:: https://img.shields.io/pypi/v/metapretty.svg
        :target: https://pypi.python.org/pypi/metapretty

.. image:: https://img.shields.io/travis/kazimuth/metapretty.svg
        :target: https://travis-ci.org/kazimuth/metapretty

.. image:: https://readthedocs.org/projects/metapretty/badge/?version=latest
        :target: https://metapretty.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Metapretty is a tool for easily configuring pretty unix UIs.


* Free software: GNU General Public License v3
* Documentation: https://metapretty.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
